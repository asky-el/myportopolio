let configSlider = {
    autoplay: true,
    loop: true,
    responsive: {
        0: {
            items: 1
        },
        600: {
            items: 1
        },
        1000: {
            items: 1
        }
    }
}
let configTestimoni = {
    loop: true,
    center: true,
    items: 1,
    margin: 0,
    autoplay: true,
    dots: true,
    autoplayTimeout: 8500,
    smartSpeed: 450,
    responsive: {
        0: {
            items: 1
        },
        768: {
            items: 1
        },
        1170: {
            items: 1
        }
    }
}
if (typeof ESD !== 'undefined') {
    ESD.add_action('hook_slider', function() {
        configSlider.loop = false;
        $('#owl-slider').owlCarousel(configSlider);
    });
    ESD.add_action('hook_slider_testimoni', function() {
        configTestimoni.loop = false;
        $('#owl-testimoni').owlCarousel(configTestimoni);
    });
}

if (typeof esd_theme === "function") {
    esd_theme = new esd_theme();
    esd_theme.contact_us('.esd-contact-form');
}



$(document).ready(function() {
    $('#owl-slider').owlCarousel(configSlider);
    $('#owl-testimoni').owlCarousel(configTestimoni);

    if (typeof esd_theme === "function") {
        esd_theme = new esd_theme();
        esd_theme.contact_us('.esd-contact-form');
    }
    /* video player */
    let ivp = $('.ivplayer');
    let vidSrc = $('[data-ivp-video]', ivp).attr('href');
    if (typeof vidSrc !== typeof undefined) {
        if (vidSrc === '' || vidSrc.length < 4) {
            ivp.find('[data-fancybox]').removeAttr('href');
            ivp.find('[data-fancybox]').removeAttr('data-fancybox');
            ivp.find('.middle-video').remove();
        }
    }

    /* Mobile Navigation */
    if ($('#navbarResponsive').length) {
        var $mobile_nav = $('#navbarResponsive').clone().prop({
            id: 'mobile-nav'
        });
        $mobile_nav.find('> ul.class-remove').attr({
            'class': '',
            'id': ''
        });
        $('body').append($mobile_nav);
        $('body').prepend('<button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>');
        $('body').append('<div id="mobile-body-overly"></div>');
        $('#mobile-nav').find('.menu-has-children').prepend('<i class="fa fa-chevron-down"></i>');

        $(document).on('click', '.menu-has-children i', function(e) {
            $(this).next().toggleClass('menu-item-active');
            $(this).nextAll('ul').eq(0).slideToggle();
            $(this).toggleClass("fa-chevron-up fa-chevron-down");
        });

        $(document).on('click', '#mobile-nav-toggle', function(e) {
            $('body').toggleClass('mobile-nav-active');
            $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
            $('#mobile-body-overly').toggle();
        });

        $(document).click(function(e) {
            var container = $("#mobile-nav, #mobile-nav-toggle");
            if (!container.is(e.target) && container.has(e.target).length === 0) {
                if ($('body').hasClass('mobile-nav-active')) {
                    $('body').removeClass('mobile-nav-active');
                    $('#mobile-nav-toggle i').toggleClass('fa-times fa-bars');
                    $('#mobile-body-overly').fadeOut();
                }
            }
        });
    } else if ($("#mobile-nav, #mobile-nav-toggle").length) {
        $("#mobile-nav, #mobile-nav-toggle").hide();
    }

    function openNav() {
        document.getElementById('mobile-nav').style.width = '50%';
    }

    function closeNav() {
        document.getElementById('mobile-nav').style.width = '0px';
    }


    $(window).scroll(function() {
        var scroll = $(window).scrollTop();
        if (scroll > 0) {
            $(".navbar-expand-lg").css("background: transparent linear-gradient(to bottom right, #CC6600 0%, #CDB79E 85%)");
        } else {
            $(".navbar-expand-lg").css("background: transparent linear-gradient(to bottom right, #CC6600 0%, #CDB79E 85%)");
        }
    });

    $(".fancybox-gallery").map(function() {
        let imgSrc = $(this).attr('src');
        $(this).closest('a').attr('href', imgSrc);
    });

    /*Back to top*/
    $(window).scroll(function() {
        if ($(this).scrollTop() > 0) {
            $('.back-to-top').fadeIn('slow');
            $('#header').addClass('header-fixed');
        } else {
            $('.back-to-top').fadeOut('slow');
            $('#header').removeClass('header-fixed');
        }
    });

    $('.back-to-top').click(function() {
        $('html, body').animate({
            scrollTop: 0
        }, 1500, 'easeInOutExpo');
        return false;
    });

    AOS.init({
        duration: 1200,
    });

});